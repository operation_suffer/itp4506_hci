<?php
// Check if the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["uname"];
    $password = $_POST["psw"];

    // You can replace this with your own validation logic
    if ($username === "user" && $password === "user") {
        // User authentication successful

        // Optionally, set a session variable to keep the user logged in
        session_start();
        $_SESSION["username"] = $username;

        // Redirect the user to a secure page (e.g., dashboard.php)
        header("Location: index.php");
        exit;

    } elseif ($username === "restaurant" && $password === "restaurant") {
        // User authentication successful

        // Optionally, set a session variable to keep the user logged in
        session_start();
        $_SESSION["username"] = $username;

        // Redirect the user to a secure page (e.g., dashboard.php)
        header("Location: restaurant_index.php");
        exit;

    } elseif ($username === "deliver" && $password === "deliver") {
        // User authentication successful

        // Optionally, set a session variable to keep the user logged in
        session_start();
        $_SESSION["username"] = $username;

        // Redirect the user to a secure page (e.g., dashboard.php)
        header("Location: delivery_index.php");
        exit;

    } else {
        // Login failed
        echo "Login failed. Please check your username and password.";
    }
}
?>
