<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout</title>
    <link href="css/style.css" rel="stylesheet"/>
</head>
<body>

<?php
session_start(); // Start the session
$username = null;

// Check if the user is logged in (i.e., if the session variable is set)
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];

    // Redirect based on the role (assuming roles are "restaurant" and "delivery")
    if ($username == "restaurant") {
        header("Location: restaurant_index.php");
        exit();
    } elseif ($username == "delivery") {
        header("Location: delivery_index.php");
        exit();
    }

    // Redirect the logged-in user directly to the checkout page
    header("Location: checkout.php");
    exit();
}
?>

<div class="container">
    <div class="checkoutLayout">


        <div class="returnCart">
            <a href="\itp4506_hci\ITP4506_HCI\user_order.php" id="keep-shopping">Keep Shopping</a>
            <h1>List Product in Cart</h1>
            <div class="list">

                <div class="item">
                    <img src="italian/1.PNG">
                    <div class="info">
                        <div class="name">PRODUCT 1</div>
                        <div class="price">$12</div>
                    </div>
                    <div class="quantity">2</div>
                    <div class="returnPrice">$24</div>
                </div>

            </div>
        </div>


        <div class="logincheck">
            <?php if ($username) { ?>
                <!-- User is logged in, proceed to checkout -->
                <h1>Checkout</h1>
                <div class="form">
                    <!-- Your checkout form goes here -->
                </div>
            <?php } else { ?>
                <!-- User is not logged in, provide options to register or continue as a guest -->
                <h1>Welcome to Checkout</h1>
                <p>You are not logged in. Please register or proceed as a guest to complete your purchase.</p>
                <!-- Update the links in your PHP file with the added classes -->
                <div class="rs">
                    <a href="register.php" class="register">Register</a>
                    <a href="checkout.php" class="guest">Continue as Guest</a>
                </div>

            <?php } ?>
        </div>
    </div>
</div>

</body>
</html>
<body>

</body>
</html>