<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Dish</title>
    <style>
        .update_dish body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .update_dish form {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            width: 400px;
        }

        .update_dish label {
            display: block;
            margin-bottom: 8px;
            font-weight: bold;
        }

        .update_dish input,
        .update_dish textarea {
            width: 100%;
            padding: 8px;
            margin-bottom: 16px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 14px;
        }

        .update_dish button {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            padding: 10px 15px;
            font-size: 16px;
            cursor: pointer;
            border-radius: 4px;
        }

        .update_dish button:hover {
            background-color: #45a049;
        }
    </style>


</head>
<body>

<?php
session_start();

// Check if the user is logged in
if (!isset($_SESSION["username"]) || $_SESSION["username"] == "user" || $_SESSION["username"] == null) {
    header("Location: index.php");
    exit();
}

// Check if the dish ID is provided in the URL
if (isset($_GET['dish'])) {
    $dishId = $_GET['dish'];

    // Load the existing menu data
    $menuJson = file_get_contents('italian/italian_menu.json');
    $menuData = json_decode($menuJson, true);

    // Check if the dish with the given ID exists
    if (isset($menuData[$dishId])) {
        // Retrieve the dish details
        $dish = $menuData[$dishId];
    } else {
        // Dish not found
        echo "Error: Dish not found.";
        exit();
    }
} else {
    // Dish ID not provided
    echo "Error: Dish ID not provided.";
    exit();
}

// Handle the form submission for updating the dish
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Validate and process the form data
    // (Add your validation and sanitization logic here)

    // Update the dish details
    $menuData[$dishId]['name'] = $_POST['dishName'];
    $menuData[$dishId]['price'] = floatval($_POST['dishPrice']);
    $menuData[$dishId]['description'] = $_POST['dishDescription'];

    // Check if a new image file is uploaded
    if ($_FILES['dishImage']['size'] > 0) {
        $imageFileName = $_FILES['dishImage']['name'];
        $imageFilePath = 'uploads/' . $imageFileName;

        // Move the uploaded file to the desired directory
        move_uploaded_file($_FILES['dishImage']['tmp_name'], $imageFilePath);

        // Update the dish image file path
        $menuData[$dishId]['images'] = $imageFilePath;
    }

    // Save the updated menu back to the JSON file
    file_put_contents('italian/italian_menu.json', json_encode($menuData, JSON_PRETTY_PRINT));

    // Redirect back to the menu page after editing
    header("Location: restaurant_menu.php");
    exit();
}
?>
    <?php
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];
        // Redirect based on the role (assuming roles are "restaurant" and "delivery")
        if ($username == "user") {
            header("Location: index.php");
            exit();
        } elseif ($username == "delivery") {
            header("Location: delivery_index.php");
            exit();
        }
    } else {
        header("Location: index.php");
    }

    // Load the JSON data
    $menuJson = file_get_contents('italian/italian_menu.json');
    $menuData = json_decode($menuJson, true);
    ?>

    <?php include 're_header.php'; ?>

<!-- Your HTML form for editing the dish -->
<form method="post" action="" enctype="multipart/form-data" class="update_dish">
    <label for="dishName">Dish Name:</label>
    <input type="text" id="dishName" name="dishName" value="<?= $dish['name'] ?>" required>

    <label for="dishPrice">Dish Price:</label>
    <input type="number" id="dishPrice" name="dishPrice" value="<?= $dish['price'] ?>" required>

    <label for="dishDescription">Dish Description:</label>
    <textarea id="dishDescription" name="dishDescription" required><?= $dish['description'] ?></textarea>

    <label for="dishType">Dish Type:</label>
    <select id="dishType" name="dishType" required>
        <option value="main" <?= ($dish['type'] == 'main') ? 'selected' : '' ?>>Main</option>
        <option value="desert" <?= ($dish['type'] == 'desert') ? 'selected' : '' ?>>Desert</option>
        <option value="drink" <?= ($dish['type'] == 'drink') ? 'selected' : '' ?>>Drink</option>
    </select>

    <label for="dishImage">Dish Image:</label>
    <input type="file" id="dishImage" name="dishImage">

    <button type="submit">Update Dish</button>
</form>

</body>
</html>
