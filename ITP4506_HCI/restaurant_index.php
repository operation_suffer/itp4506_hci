<!DOCTYPE php>
<php lang="en" xml:lang="en">

    <?php
    session_start(); // Start the session
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];
        // Redirect based on the role (assuming roles are "restaurant" and "delivery")
        if ($username == "user") {
            header("Location: index.php");
            exit();
        } elseif ($username == "delivery") {
            header("Location: delivery_index.php");
            exit();
        }
    } else {
        header("Location: index.php");
    }

    ?>

    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <!-- Site Metas -->
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <title>Yummy Restaurant Group Limited</title>

        <!-- bootstrap core css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

        <!-- fonts style -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">
        <!--owl slider stylesheet -->
        <link rel="stylesheet" type="text/css" +
              href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
        <!-- nice select -->
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" +
              integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous"/>
        <!-- font awesome style -->
        <link href="css/font-awesome.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet"/>
        <!-- responsive style -->
        <link href="css/responsive.css" rel="stylesheet"/>
    </head>

    <body>
    <div class="re_hero_area">
        <!-- header section strats -->
        <header class="header_section">
            <div class="header_top">
                <div class="container-fluid header_top_container">
                    <a class="navbar-brand " href="restaurant_index.php">Yummy Restaurant <span>Group Limited</span>
                    </a>
                    <div class="contact_nav">
                        <a href="">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <span>
                Location
              </span>
                        </a>
                        <a href="">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>
                Call : +01 123455678990
              </span>
                        </a>
                        <a href="">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <span>
                demo@gmail.com
              </span>
                        </a>
                    </div>
                    <div class="social_box">
                        <a href="">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="header_bottom">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg custom_nav-container ">
                        <a class="navbar-brand navbar_brand_mobile" href="restaurant_index.php"> Yummy Restaurant <span>Group Limited</span>
                        </a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                +data-target="#navbarSupportedContent" +
                                aria-controls="navbarSupportedContent" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span class=""> </span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav  ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="restaurant_index.php">Home<span
                                                class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="restaurant_order.php">Order</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="restaurant_record.php">Record</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="restaurant_menu.php">Menu</a>
                                </li>
                                <?php
                                if ($username) {
                                    // User is logged in, show the username, logout button, history, and order buttons
                                    echo '<li class="nav-item" id="registerMenuItem">';
                                    echo '<a class="nav-link" href="#">';
                                    echo '<i class="fa fa-user" aria-hidden="true"></i>';
                                    echo '<span>';
                                    echo 'User: ' . $username;
                                    echo '</span>';
                                    echo '</a>';
                                    echo '</li>';
                                    echo '<li class="nav-item" id="logoutMenuItem">';
                                    echo '<a class="nav-link" href="logout.php">Logout</a>';
                                    echo '</li>';
                                } else {
                                    // User is not logged in, show the login item and hide the username, history, and order buttons
                                    echo '<li class="nav-item" id="registerMenuItem">';
                                    echo '<a class="nav-link" href="login.php">';
                                    echo '<i class="fa fa-user" aria-hidden="true"></i>';
                                    echo '<span>';
                                    echo 'Login';
                                    echo '</span>';
                                    echo '</a>';
                                    echo '</li>';
                                }
                                ?>
                                </li>
                                <form class="form-inline">
                                    <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- end header section -->

        <!-- slider section -->
        <section class="slider_section ">
            <div id="customCarousel1" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container ">
                            <div class="detail-box">
                                <h1>
                                    We Cook <br/>
                                    The Things of Arts <br/>
                                    You Desire
                                </h1>
                                <div class="btn-box">
                                    <a href="" class="btn1">
                                        Read More
                                    </a>
                                    <a href="" class="btn2">
                                        Contact Us
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container ">
                            <div class="detail-box">
                                <h1>
                                    We Provide <br/>
                                    The Best of The Best <br/>
                                    Dream Up
                                </h1>
                                <div class="btn-box">
                                    <a href="" class="btn1">
                                        Read More
                                    </a>
                                    <a href="" class="btn2">
                                        Contact Us
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container ">
                            <div class="detail-box">
                                <h1>
                                    We Serve <br/>
                                    The Quality <br/>
                                    Improvements
                                </h1>
                                <div class="btn-box">
                                    <a href="" class="btn1">
                                        Read More
                                    </a>
                                    <a href="" class="btn2">
                                        Contact Us
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel_btn-box">
                    <a class="carousel-control-prev" href="#customCarousel1" role="button" data-slide="prev">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#customCarousel1" role="button" data-slide="next">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>

        <!-- end slider section -->
    </div>

    <!-- about section -->

    <section class="about_section layout_padding">
        <div class="container  ">
            <div class="row">
                <div class="col-md-6">
                    <div class="detail-box">
                        <div class="heading_container">
                            <h2>Welcome to <span>Yummy Restaurant Group Limited</span></h2>
                        </div>
                        <p>
                            Yummy Restaurant Group Limited is the epitome of culinary excellence.
                            From our humble beginnings to our current network of award-winning restaurants
                            we've been dedicated to crafting unforgettable dining experiences.
                            Join us on a journey of taste, innovation, and community commitment.
                            At Yummy, every meal is an adventure. Come hungry, leave Yummy!
                        </p>
                        <a href="">
                            Read More
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end about section -->

    <!-- contact section -->
    <section class="contact_section ">
        <div class="container">
            <div class="heading_container heading_center">
                <h2>Get In <span>Touch</span></h2>
            </div>
            <div class="row">
                <div class="col-md-6 px-0">
                    <div class="form_container">
                        <form action="">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" placeholder="Your Name"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-lg-6">
                                    <input type="text" class="form-control" placeholder="Phone Number"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <select name="" id="" class="form-control wide">
                                        <option value="">Select Service</option>
                                        <option value="">Service 1</option>
                                        <option value="">Service 2</option>
                                        <option value="">Service 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="email" class="form-control" placeholder="Email"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="message-box form-control" placeholder="Message"/>
                                </div>
                            </div>
                            <div class="btn_box">
                                <button>
                                    SEND
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 px-0">
                    <div class="map_container">
                        <div class="map">
                            <div id="googleMap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end contact section -->

    <?php include 'footer.php'; ?>
    </body>
