<!DOCTYPE php>
<php lang="en" xml:lang="en">


    <?php
    session_start(); // Start the session
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];
    }
    // Redirect based on the role (assuming roles are "restaurant" and "delivery")
    if ($username == "restaurant") {
        header("Location: restaurant_index.php");
        exit();
    } elseif ($username == "delivery") {
        header("Location: delivery_index.php");
        exit();
    }
    ?>

    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <!-- Site Metas -->
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <title>Yummy Restaurant Group Limited</title>

        <!-- bootstrap core css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
        <!-- login css -->
        <link rel="stylesheet" type="text/css" href="css/login.css"/>

        <!-- fonts style -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">
        <!--owl slider stylesheet -->
        <link rel="stylesheet" type="text/css" +
              href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
        <!-- nice select -->
        <link rel="stylesheet" +
              href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" +
              integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous"/>
        <!-- font awesome style -->
        <link href="css/font-awesome.min.css" rel="stylesheet"/>

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet"/>
        <!-- responsive style -->
        <link href="css/responsive.css" rel="stylesheet"/>
    </head>

    <body class="sub_page">
    <div class="hero_area">
        <!-- header section strats -->
        <header class="header_section">
            <div class="header_top">
                <div class="container-fluid header_top_container">
                    <a class="navbar-brand " href="index.php">Yummy Restaurant <span>Group Limited</span> </a>
                    <div class="contact_nav">
                        <a href="">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <span>
                Location
                </span>
                        </a>
                        <a href="">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>
                Call : +01 123455678990
                </span>
                        </a>
                        <a href="">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <span>
                demo@gmail.com
                </span>
                        </a>
                    </div>
                    <div class="social_box">
                        <a href="">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="header_bottom">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg custom_nav-container ">
                        <a class="navbar-brand navbar_brand_mobile" href="index.php"> Yummy Restaurant <span>Group Limited</span>
                        </a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" +
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" +
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class=""> </span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav  ">
                                <li class="nav-item ">
                                    <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="order.php">Order</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about.php">About</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="contact.php">Contact Us</a>
                                </li>
                                <?php
                                if ($username) {
                                    // User is logged in, show the username, logout button, history, and order buttons
                                    echo '<li class="nav-item" id="registerMenuItem">';
                                    echo '<a class="nav-link" href="#">';
                                    echo '<i class="fa fa-user" aria-hidden="true"></i>';
                                    echo '<span>';
                                    echo 'User: ' . $username;
                                    echo '</span>';
                                    echo '</a>';
                                    echo '</li>';
                                    echo '<li class="nav-item" id="orderMenuItem">';
                                    echo '<a class="nav-link" href="orders_tracking.php">Orders Tracking</a>';
                                    echo '</li>';
                                    echo '<li class="nav-item" id="logoutMenuItem">';
                                    echo '<a class="nav-link" href="logout.php">Logout</a>';
                                    echo '</li>';
                                } else {
                                    // User is not logged in, show the login item and hide the username, history, and order buttons
                                    echo '<li class="nav-item" id="registerMenuItem">';
                                    echo '<a class="nav-link" href="login.php">';
                                    echo '<i class="fa fa-user" aria-hidden="true"></i>';
                                    echo '<span>';
                                    echo 'Login';
                                    echo '</span>';
                                    echo '</a>';
                                    echo '</li>';
                                }
                                ?>
                                </li>
                                <form class="form-inline">
                                    <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- end header section -->

    </div>
    <body class="sub_page">
    <div class="hero_area">
        <!-- header section starts -->
        <!-- ... (your header code) ... -->
    </div>

    <div class="registration-success">
        <h2>Thank You for Applying to Restaurant Partner!!</h2>
        <p>We appreciate your interest in becoming part of our team at Yummy Restaurant Group Limited.</p>
        <p>To complete your application process, please check your email and follow the instructions sent to you.</p>
        <p>If you don't receive an email within a few days, kindly check your spam folder.</p>
        <p><a href="index.php">Back to homepage</a></p>
    </div>


    <!-- info section -->
    <section class="info_section">
        <!-- ... (your info section code) ... -->
    </section>

    <!-- jQery, popper js, bootstrap js, owl slider, nice select, custom js, Google Map -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- ... (other script references) ... -->
    </body>


    <!-- info section -->
    <section class="info_section ">
        <div class="info_container layout_padding2">
            <div class="container">
                <div class="info_logo">
                    <a class="navbar-brand" href="index.php"> Yummy Restaurant <span>Group Limited</span> </a>
                </div>
                <div class="info_main">
                    <div class="row">
                        <div class="col-md-3 col-lg-2">
                            <div class="info_link-box">
                                <h5>
                                    Useful Link
                                </h5>
                                <ul>
                                    <li class=" active">
                                        <a class="" href="index.php">Home<span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="">
                                        <a class="" href="about.php">About</a>
                                    </li>
                                    <li class="">
                                        <a class="" href="contact.php">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 mx-auto  ">
                            <h5>
                                social media
                            </h5>
                            <div class="social_box">
                                <a href="#">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>
                                Our Restaurant
                            </h5>
                            <p>
                                3/F Trend Plaza South Wing,
                                Tuen Mun, Hong Kong
                            </p>
                        </div>
                    </div>
                </div>
                <div class="info_bottom">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="info_contact ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="#" class="link-box">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span>
                            Location
                        </span>
                                        </a>
                                    </div>
                                    <div class="col-md-5">
                                        <a href="#" class="link-box">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span>
                            Call +01 1234567890
                        </span>
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="#" class="link-box">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>
                            demo@gmail.com
                        </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info_form ">
                                <form action="">
                                    <input type="email" placeholder="Enter Your Email"/>
                                    <button>
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end info section -->

    <!-- jQery -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" +
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.js"></script>
    <!-- owl slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!--  OwlCarousel 2 - Filter -->
    <script src="https://huynhhuynh.github.io/owlcarousel2-filter/dist/owlcarousel2-filter.min.js"></script>
    <!-- nice select -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js" +
            integrity="sha256-Zr3vByTlMGQhvMfgkQ5BtWRSKBGa2QlspKYJnkjZTmo=" crossorigin="anonymous"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
    </script>
    <!-- End Google Map -->

    </body>
