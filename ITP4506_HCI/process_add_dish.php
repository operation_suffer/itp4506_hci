<?php
// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $dishName = $_POST["dishName"];
    $price = floatval($_POST["price"]);
    $description = $_POST["description"];

    // Handle image upload
    $targetDir = "italian/";  // Change this to your desired directory for storing uploaded images
    $targetFile = $targetDir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

    // Check if the file is an actual image
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if ($check === false) {
        die("Error: File is not an image.");
    }

    // Check if file already exists
    if (file_exists($targetFile)) {
        die("Error: File already exists.");
    }

    // Allow certain image file formats
    $allowedFormats = ["jpg", "jpeg", "png", "gif", "webp"];
    if (!in_array($imageFileType, $allowedFormats)) {
        die("Error: Only JPG, JPEG, webp, PNG, and GIF files are allowed.");
    }

    // Move the uploaded file to the specified directory
    if (move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile)) {
        // Read the existing menu data from json file
        $menuData = json_decode(file_get_contents("italian/italian_menu.json"), true);

        // Generate a new dish ID (assuming IDs are unique)
        $newDishId = count($menuData);

        // Create a new dish entry
        $newDish = [
            "id" => $newDishId,
            "name" => $dishName,
            "images" => basename($_FILES["image"]["name"]),
            "price" => $price,
            "type" => "main",  // You may want to adjust the dish type as needed
            "description" => $description
        ];

        // Add the new dish to the menu data
        $menuData[] = $newDish;

        // Save the updated menu data back to the json file
        file_put_contents("italian/italian_menu.json", json_encode($menuData, JSON_PRETTY_PRINT));

        // Display success message
        echo '<script>alert("Dish added successfully!");</script>';
        header("Location: restaurant_menu.php");
    } else {
        die("Error: There was an error uploading the file.");
    }
} else {
    // Redirect to the add dish page if accessed without form submission
    header("Location: restaurant_menu.php");
    exit();
}
?>
