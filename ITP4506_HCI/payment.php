<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment</title>
    <link href="css/style.css" rel="stylesheet"/>
</head>
<body>

<?php
session_start(); // Start the session
$username = null;

// Check if the user is logged in (i.e., if the session variable is set)
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
}
// Redirect based on the role (assuming roles are "restaurant" and "delivery")
if ($username == "restaurant") {
    header("Location: restaurant_index.php");
    exit();
} elseif ($username == "delivery") {
    header("Location: delivery_index.php");
    exit();
}
?>

<div class="containerP">
    <div class="checkoutLayout">


        <div class="returnCart">
            <a href="\itp4506_hci\ITP4506_HCI\order.php" id="keep-shopping">Keep Shopping</a>
            <h1>List Product in Cart</h1>
            <div class="list">

                <div class="item">
                    <img src="italian/1.PNG">
                    <div class="info">
                        <div class="name">PRODUCT 1</div>
                        <div class="price">$12</div>
                    </div>
                    <div class="quantity">2</div>
                    <div class="returnPrice">$24</div>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-75">
                <div class="container">
                    <form action="food_deliver.php">

                        <div class="row">
                            <div class="col-50">
                                <h3>Billing Address</h3>
                                <label for="fname"><i class="fa fa-user"></i> Full Name</label>
                                <input type="text" id="fname" name="firstname"
                                       value="<?php echo $username ? 'John Cena' : ''; ?>" required>
                                <label for="email"><i class="fa fa-envelope"></i> Email</label>
                                <input type="text" id="email" name="email"
                                       value="<?php echo $username ? 'john_cena@example.com' : ''; ?>" required>
                                <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                                <input type="text" id="adr" name="address"
                                       value="<?php echo $username ? '123 Main St' : ''; ?>" required>
                                <label for="city"><i class="fa fa-institution"></i> City</label>
                                <input type="text" id="district" name="district"
                                       value="<?php echo $username ? 'Kowloon City' : ''; ?>" required>
                            </div>

                            <div class="col-50">
                                <h3>Payment</h3>
                                <label for="fname">Accepted Cards</label>
                                <div class="icon-container">
                                    <img src="images/mastercard.png" alt="Master Card Icon"
                                         style="width: 20px; height: auto;">
                                    <img src="images/visa.png" alt="Visa Card Icon" style="width: 20px; height: auto;">
                                    <img src="images/unionpay.png" alt="Unionpay Card Icon"
                                         style="width: 20px; height: auto;">
                                </div>
                                <label for="cname">Name on Card</label>
                                <input type="text" id="cname" name="cardname"
                                       value="<?php echo $username ? 'John Cena' : ''; ?>" required>
                                <label for="ccnum">Credit card number</label>
                                <input type="text" id="ccnum" name="cardnumber" oninput="formatCreditCardNumber(this)"
                                       value="<?php echo $username ? '1111222233334444' : ''; ?>" required>
                                <script>
                                    function formatCreditCardNumber(input) {
                                        // Remove existing hyphens and spaces from the input
                                        const cleanedValue = input.value.replace(/[\s-]/g, '');

                                        // Split the cleaned value into groups of four characters
                                        const formattedValue = cleanedValue.match(/.{1,4}/g);

                                        // Join the groups with hyphens and update the input value
                                        if (formattedValue) {
                                            input.value = formattedValue.join('-');
                                        }
                                    }
                                </script>

                                <label for="expmonth">Exp Month</label>
                                <input type="text" id="expmonth" name="expmonth"
                                       value="<?php echo $username ? 'September' : ''; ?>" required>

                                <div class="row">
                                    <div class="col-50">
                                        <label for="expyear">Exp Year</label>
                                        <input type="text" id="expyear" name="expyear"
                                               value="<?php echo $username ? '2018' : ''; ?>" required>
                                    </div>
                                    <div class="col-50">
                                        <label for="cvv">CVV</label>
                                        <input type="text" id="cvv" name="cvv"
                                               value="<?php echo $username ? '352' : ''; ?>" required>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <label>
                            <input type="checkbox" checked="checked" name="sameadr"> Shipping address same as billing
                        </label>
                        <button type="submit" class="buttonCheckout">Confirm Payment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<body>

</body>
</html>