<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delivery</title>
    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <!-- fonts style -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">
    <!--owl slider stylesheet -->
    <link rel="stylesheet" type="text/css" +
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
    <!-- nice select -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" +
          integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous"/>
    <!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet"/>
    <!-- responsive style -->
    <link href="css/responsive.css" rel="stylesheet"/>
</head>

<?php
session_start(); // Start the session
$username = null;

// Check if the user is logged in (i.e., if the session variable is set)
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
}
// Redirect based on the role (assuming roles are "restaurant" and "delivery")
if ($username == "restaurant") {
    header("Location: restaurant_index.php");
    exit();
} elseif ($username == "delivery") {
    header("Location: delivery_index.php");
    exit();
}
?>

<body class="sub_page">
<div class="container">
    <div class="checkoutLayout">
        <div class="returnCart">
            <a href="order.php" id="keep-shopping">Keep Shopping</a>
            <h1>List Product in Delivery</h1>
            <div class="list">
                <div class="item">

                    <img src="italian/1.PNG">
                    <div class="info">
                        <div class="orderNumber">ORDER #25142589849</div>
                        <div class="name">PRODUCT 1</div>
                        <div class="price">$12</div>
                    </div>
                    <div class="quantity">2</div>
                    <div class="returnPrice">$24</div>
                </div>
            </div>
        </div>

        <div class="right">
            <h1>Delivery</h1>
            <!-- Show google map here -->
            <div class="map_container">
                <div class="map">
                    <div id="googleMap" style="width: auto; height: 500px;"></div>
                </div>
            </div>
            <div class="deliveryInfo">
                <div class="status">Status: Preparing</div>
                <div class="date">Estimated Delivery Time: 18:33</div>
            </div>

            <div class="return">
                <div class="row">
                    <div>Total Quantity</div>
                    <div class="totalQuantity">2</div>
                </div>
                <div class="row">
                    <div>Total Price</div>
                    <div class="totalPrice">$24.0</div>
                </div>
                <button class="btn" onclick="redirectToIndex()">Confirm</button>
                <script>
                    function redirectToIndex() {
                        window.location.href = 'index.php';
                    }
                </script>
            </div>
        </div>
        <!-- Feedback and Rating Section -->
        <div class="feedback-section">
            <h2>Customer Feedback</h2>
            <form action="submit_feedback.php" method="post">
                <label for="feedback">Your Feedback:</label>
                <textarea id="feedback" name="feedback" rows="4" required></textarea>

                <label for="rating">Rating (1-5):</label>
                <input type="number" id="rating" name="rating" min="1" max="5" required>

                <button type="submit" class="btn">Submit Feedback</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<body>

<!-- jQery -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- popper js -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" +
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.js"></script>
<!-- owl slider -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<!--  OwlCarousel 2 - Filter -->
<script src="https://huynhhuynh.github.io/owlcarousel2-filter/dist/owlcarousel2-filter.min.js"></script>
<!-- nice select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js" +
        integrity="sha256-Zr3vByTlMGQhvMfgkQ5BtWRSKBGa2QlspKYJnkjZTmo=" crossorigin="anonymous"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
<!-- Google Map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
</script>
<!-- End Google Map -->
</body>
</html>