let openShopping = document.querySelector('.shopping');
let closeShopping = document.querySelector('.closeShopping');
let list = document.querySelector('.list');
let listCart = document.querySelector('.listCart');
let body = document.querySelector('body');
let total = document.querySelector('.total');
let quantity = document.querySelector('.quantity');

total.addEventListener('click', () => {
    // Check if the cart is not empty before proceeding to the checkout
    if (cartIsNotEmpty()) {
        // Redirect to checkout_logincheck.php
        window.location.href = 'checkout_logincheck.php';
    } else {
        // Provide a message or take alternative actions if the cart is empty
        alert('Your cart is empty. Please add items before proceeding to checkout.');
    }
});


function cartIsNotEmpty() {

    return Object.keys(listCarts).length > 0;
}


openShopping.addEventListener('click', ()=>{
    body.classList.add('active');
})
closeShopping.addEventListener('click', ()=>{
    body.classList.remove('active');
})

let products; // Declare the variable to hold the products

// Fetch products.json asynchronously
fetch('restaurant/restaurant.json')
    .then(response => response.json())
    .then(data => {
        products = data; // Assign the fetched data to the products variable
        initApp(); // Call initApp after the data is loaded
    })
    .catch(error => console.error('Error fetching products:', error));


let listCarts  = [];
function initApp(){
    products.forEach((value, key) =>{
        let newDiv = document.createElement('div');
        newDiv.classList.add('item');
        newDiv.innerHTML = `
<img src="restaurant/${value.images}" onclick="redirectToLink('${value.link}')">
            <div class="restaurant_name">${value.name}</div>
            <div class="Type">${value.type}</div>
            <div class="district">${value.district}</div>`;
        list.appendChild(newDiv);
    })
}
initApp();
function redirectToLink(link) {
    window.location.href = link;
}
function addToCart(key){
    if(listCarts[key] == null){
        // copy product form list to list cart
        listCarts[key] = JSON.parse(JSON.stringify(products[key]));
        listCarts[key].quantity = 1;
    }
    reloadCart();
}
function reloadCart(){
    listCart.innerHTML = '';
    let count = 0;
    let totalPrice = 0;
    listCarts.forEach((value, key)=>{
        totalPrice = totalPrice + value.price;
        count = count + value.quantity;
        if(value != null){
            let newDiv = document.createElement('li');
            newDiv.innerHTML = `
                <div><img src="${value.images}"/></div>
                <div>${value.name}</div>
                <div>${value.price.toLocaleString()}</div>
                <div>
                    <button onclick="changeQuantity(${key}, ${value.quantity - 1})">-</button>
                    <div class="count">${value.quantity}</div>
                    <button onclick="changeQuantity(${key}, ${value.quantity + 1})">+</button>
                </div>`;
            listCart.appendChild(newDiv);
        }
    })
    total.innerText = "Total Price: $" + totalPrice.toLocaleString() + "\n Click to Check Out";
    quantity.innerText = count;
}

function changeQuantity(key, quantity){
    if(quantity == 0){
        delete listCarts[key];
    }else{
        listCarts[key].quantity = quantity;
        listCarts[key].price = quantity * products[key].price;
    }
    reloadCart();
}