<?php
session_start();

if (isset($_GET['orderNumber'])) {
    $orderNumber = $_GET['orderNumber'];

    // Read the existing orders from the JSON file
    $orders = json_decode(file_get_contents('delivery_order.json'), true);

    // Check if the provided order index exists in the array
    if (isset($orders[$orderNumber]) && $orders[$orderNumber]["status"] == "In Progress") {
        // Save the original order details
        $originalOrder = $orders[$orderNumber];

        // Remove the current order with "Wait for deliver" status
        unset($orders[$orderNumber]);

        // Add a new order with the original details and "In Progress" status
        $newOrder = array(
            "orderNumber" => $originalOrder["orderNumber"],
            "fromAddress" => $originalOrder["fromAddress"],
            "toAddress" => $originalOrder["toAddress"],
            "deliveryTime" => $originalOrder["deliveryTime"],
            "status" => "Delivered"
        );

        // Add the new order to the array
        $orders[] = $newOrder;

        // Save the updated orders back to the JSON file
        file_put_contents('delivery_order.json', json_encode($orders));

        // Send a success response
        echo "Success";
    } else {
        // Send an error response if the order index is invalid or status is not "Wait for deliver"
        echo "Error: Invalid order index or status.";
    }
} else {
    // Send an error response if the order number is not provided
    echo "Error: Order number not provided.";
}
?>
