<!DOCTYPE php>
<php lang="en" xml:lang="en">


    <?php
    session_start(); // Start the session
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];
    }
    // Redirect based on the role (assuming roles are "restaurant" and "delivery")
    if ($username == "user") {
        header("Location: index.php");
        exit();
    } elseif ($username == "delivery") {
        header("Location: delivery_index.php");
        exit();
    }
    ?>
<?php include 're_header.php'; ?>

    <div class="container">
        <h2>Add New Dish</h2>
        <!-- Add your form for adding a new dish here -->
        <form method="post" action="process_add_dish.php" enctype="multipart/form-data">
            <!-- Add your form fields for dish details (e.g., name, price, description) -->
            <label for="dishName">Dish Name:</label>
            <input type="text" name="dishName" required />

            <label for="price">Price:</label>
            <input type="text" name="price" required />

            <label for="description">Description:</label>
            <textarea name="description" rows="4" required></textarea>

            <!-- Add the input field for uploading an image -->
            <label for="image">Upload Image:</label>
            <input type="file" name="image" accept="image/*" required />

            <!-- Add any other form fields as needed -->

            <input type="submit" value="Add Dish" class="btn btn-primary" />
        </form>
    </div>

<!-- Include the footer section -->
<?php include 'footer.php'; ?>

<!-- Include any necessary scripts at the end of the body -->

</body>

</html>
</php>
