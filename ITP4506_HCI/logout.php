<?php
// Start the session
session_start();

// Unset all session variables
session_unset();

// Destroy the session
session_destroy();

// Redirect to a page after logout (e.g., index.php)
header("Location: index.php");
exit;
