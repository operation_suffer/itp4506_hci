<!DOCTYPE php>
<php lang="en" xml:lang="en">
    <?php
    session_start(); // Start the session
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];
        // Redirect based on the role (assuming roles are "restaurant" and "delivery")
        if ($username == "user") {
            header("Location: index.php");
            exit();
        } elseif ($username == "delivery") {
            header("Location: delivery_index.php");
            exit();
        }
    } else {
        header("Location: index.php");
    }

    // Load the JSON data
    $menuJson = file_get_contents('italian/italian_menu.json');
    $menuData = json_decode($menuJson, true);
    ?>

    <?php include 're_header.php'; ?>

    <div class="menu_button">
        <button class="btn btn-primary my-2 my-sm-0" id="addNewDishBtn" onclick="location.href='add_dish.php'">
            Add New Dish
        </button>
    </div>

    <div class="menu_items">
        <?php foreach ($menuData as $dish): ?>
            <div class="dish_item">
                <h3><?= $dish['name'] ?></h3>
                <p>Price: <?= $dish['price'] ?></p>
                <img src="italian/<?= $dish['images'] ?>" alt="<?= $dish['name'] ?>" class="dish_image">
                <p><?= $dish['type'] ?></p>
                <p><?= $dish['description'] ?></p>
                <button class="btn btn-info" onclick="location.href='edit_dish.php?dish=<?= urlencode($dish['id']) ?>'">
                    Edit Dish
                </button>
                <button class="btn btn-delete" onclick="location.href='delete_dish.php?dish=<?= urlencode($dish['id']) ?>'">
                    Delete Dish
                </button>
            </div>
        <?php endforeach; ?>
    </div>

    </body>
    <style>
        /* Add this CSS to your existing styles or in a separate CSS file */

        .menu_items {
            display: flex;
            flex-direction: column; /* Display items in a column */
        }

        .dish_item {
            border: 1px solid #ddd;
            padding: 15px;
            margin: 10px;
            width: 100%; /* Occupy full width */
            text-align: center;
        }

        .dish_image {
            width: 300px; /* Fixed width */
            height: 200px; /* Adjust height as needed */
            object-fit: cover; /* Maintain aspect ratio and cover container */
            margin-bottom: 10px;
        }

        .btn-info {
            background-color: green;
            color: #fff;
            border: 1px solid green;
        }
        .btn-delete {
            background-color: darkred;
            color: #fff;
            border: 1px solid darkred;
        }

        .btn-info:hover {
            background-color: darkgreen;
            color: #fff;
        }
        .btn-delete:hover {
            background-color: red;
            color: #fff;
        }
    </style>

    <!-- Remaining HTML content... -->
</php>
