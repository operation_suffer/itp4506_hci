<!DOCTYPE php>
<php lang="en" xml:lang="en">


    <?php
    session_start(); // Start the session
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];
        // Redirect based on the role (assuming roles are "restaurant" and "delivery")
        if ($username == "user") {
            header("Location: index.php");
            exit();
        } elseif ($username == "delivery") {
            header("Location: delivery_index.php");
            exit();
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <?php include 're_header.php'; ?>

    </div>
    <body>
    <div class="container2">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-9">
                <div class="osahan-account-page-right shadow-sm bg-white p-4 h-100">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane  fade  active show" id="orders" role="tabpanel"
                             aria-labelledby="orders-tab">
                            <h4 class="font-weight-bold mt-0 mb-4">Orders</h4>
                            <div class="bg-white card mb-4 order-list shadow-sm">
                                <div class="gold-members p-4">
                                    <a href="#">
                                    </a>
                                    <div class="media">
                                        <a href="#">
                                            <img class="mr-4" src="images/user.png" alt="Restaurant image">
                                        </a>
                                        <div class="media-body">
                                            <a href="#">
                                                <span class="float-right text-info">Estimate delivery time on 7:18 PM <i
                                                        class="icofont-check-circled text-success"></i></span>
                                            </a>
                                            <h6 class="mb-2">
                                                <a href="#"></a>
                                                <a href="#" class="text-black">John P.</a>
                                            </h6>
                                            <p class="text-gray mb-1"><i class="icofont-location-arrow"></i> ABC
                                                Building 15/F, Kowloon City
                                            </p>
                                            <p class="text-gray mb-3"><i class="icofont-list"></i> ORDER #25102589748 <i
                                                    class="icofont-clock-time ml-2"></i> Mon, Nov 12, 6:26 PM</p>
                                            <p class="text-dark">PRODUCT NAME 1 x 1, PRODUCT NAME 2 x 2, PRODUCT NAME 3
                                                x 3
                                            </p>
                                            <hr>
                                            <div class="float-right">
                                                <a class="btn btn-sm btn-outline-primary" href="#"><i
                                                        class="icofont-headphone-alt"></i> Complete</a>
                                                <a class="btn btn-sm btn-outline-primary" href="#"><i
                                                        class="icofont-headphone-alt"></i> Contact</a>
                                            </div>
                                            <p class="mb-0 text-black text-primary pt-2"><span
                                                    class="text-black font-weight-bold"> Total Paid:</span> $102
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="bg-white card mb-4 order-list shadow-sm">
                                <div class="gold-members p-4">
                                    <a href="#">
                                    </a>
                                    <div class="media">
                                        <a href="#">
                                            <img class="mr-4" src="images/user.png" alt="Restaurant image">
                                        </a>
                                        <div class="media-body">
                                            <a href="#">
                                                <span class="float-right text-info">Estimate delivery time on 7:18 PM <i
                                                        class="icofont-check-circled text-success"></i></span>
                                            </a>
                                            <h6 class="mb-2">
                                                <a href="#"></a>
                                                <a href="#" class="text-black">Ada A.</a>
                                            </h6>
                                            <p class="text-gray mb-1"><i class="icofont-location-arrow"></i> XYZ Tower
                                                20/F, Wong Tai Sin
                                            </p>
                                            <p class="text-gray mb-3"><i class="icofont-list"></i> ORDER #25102589748 <i
                                                    class="icofont-clock-time ml-2"></i> Mon, Nov 12, 6:26 PM</p>
                                            <p class="text-dark">Sushi Roll x 5, Salmon Slices x 3
                                            </p>
                                            <hr>
                                            <div class="float-right">
                                                <a class="btn btn-sm btn-outline-primary" href="#"><i
                                                        class="icofont-headphone-alt"></i> Complete</a>
                                                <a class="btn btn-sm btn-outline-primary" href="#"><i
                                                        class="icofont-headphone-alt"></i> Contact</a>
                                            </div>
                                            <p class="mb-0 text-black text-primary pt-2"><span
                                                    class="text-black font-weight-bold"> Total Paid:</span> $500
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="bg-white card  order-list shadow-sm">
                                <div class="gold-members p-4">
                                    <a href="#">
                                    </a>
                                    <div class="media">
                                        <a href="#">
                                            <img class="mr-4" src="images/user.png" alt="Restaurant image">
                                        </a>
                                        <div class="media-body">
                                            <a href="#">
                                                <span class="float-right text-info">Estimate delivery time on 7:18 PM <i
                                                        class="icofont-check-circled text-success"></i></span>
                                            </a>
                                            <h6 class="mb-2">
                                                <a href="#"></a>
                                                <a href="#" class="text-black">Calvin C.</a>
                                            </h6>
                                            <p class="text-gray mb-1"><i class="icofont-location-arrow"></i> 123 Main
                                                Street, Southern
                                            </p>
                                            <p class="text-gray mb-3"><i class="icofont-list"></i> ORDER #25102589748 <i
                                                    class="icofont-clock-time ml-2"></i> Mon, Nov 12, 6:26 PM</p>
                                            <p class="text-dark">Chicken Makhani x 5, Samosas x 1
                                            </p>
                                            <hr>
                                            <div class="float-right">
                                                <a class="btn btn-sm btn-outline-primary" href="#"><i
                                                        class="icofont-headphone-alt"></i> Complete</a>
                                                <a class="btn btn-sm btn-outline-primary" href="#"><i
                                                        class="icofont-headphone-alt"></i> Contact</a>
                                            </div>
                                            <p class="mb-0 text-black text-primary pt-2"><span
                                                    class="text-black font-weight-bold"> Total Paid:</span> $420
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </body>

    <?php include 'footer.php'; ?>

    </body>
</php>