<!DOCTYPE php>
<html lang="en" xml:lang="en">

<head>
    <!-- Add your meta tags and title here -->

    <?php
    // Include necessary PHP files and start the session
    session_start();
    $username = null;

    // Check if the user is logged in (i.e., if the session variable is set)
    if (isset($_SESSION["username"])) {
        $username = $_SESSION["username"];

        // Redirect based on the role (assuming roles are "restaurant" and "delivery")
        if ($username == "restaurant") {
            header("Location: restaurant_index.php");
            exit();
        } elseif ($username == "user") {
            header("Location: index.php");
            exit();
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <?php include 'de_header.php'; ?>

    <style>
        .de_container body, html {
            margin: 0;
            padding: 0;
        }

        .de_container body {
            font-family: Arial, sans-serif;
        }

        .de_container {
            max-width: 800px;
            margin: 50px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #fff;
        }

        .de_container h2 {
            color: #333;
        }

        .de_container ul {
            list-style: none;
            padding: 0;
        }

        .de_container li {
            border: 1px solid #eee;
            border-radius: 5px;
            margin-bottom: 10px;
            padding: 10px;
        }

        .de_container button {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            padding: 8px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            cursor: pointer;
            border-radius: 3px;
        }

        .de_container button:hover {
            background-color: #45a049;
        }

        .de_container p {
            color: #777;
        }
    </style>

    <script>
        function takeOrder(orderNumber) {
            // Send an AJAX request to update the order status and add a new order
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    // Reload the page after successfully updating the order status
                    location.reload();
                }
            };
            xhr.open("GET", "update_order_inprogress.php?orderNumber=" + encodeURIComponent(orderNumber), true);
            xhr.send();
        }
    </script>
</head>

<body>
<div class="de_container">
    <h2>Delivery Orders</h2>

    <?php
    // Read delivery_order.json and filter orders in progress or waiting for delivery
    $orders = json_decode(file_get_contents('delivery_order.json'), true);

    if (!empty($orders)) {
        echo '<ul>';
        foreach ($orders as $key => $order) {
            if ($order["status"] == "Wait for deliver") {
                echo '<li>';
                echo 'Order Number: ' . $order["orderNumber"] . '<br><br>';
                echo 'From: ' . $order["fromAddress"] . '<br>';
                echo 'To: ' . $order["toAddress"] . '<br><br>';
                echo 'Delivery Time: ' . $order["deliveryTime"] . '<br>';
                echo 'Status: ' . $order["status"] . '<br><br>';
                // Add a button for taking the order and updating the status
                echo '<button onclick="takeOrder(' . $key . ')">Take Order</button>';
                echo '</li>';
            }
        }
        echo '</ul>';
    } else {
        echo '<p>No delivery orders available.</p>';
    }
    ?>

</div>

</body>

</html>
