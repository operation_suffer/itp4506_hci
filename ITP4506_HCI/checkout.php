<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout</title>
    <link href="css/style.css" rel="stylesheet"/>
</head>
<body>

<?php
session_start(); // Start the session
$username = null;

// Check if the user is logged in (i.e., if the session variable is set)
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];

    // Redirect based on the role (assuming roles are "restaurant" and "delivery")
    if ($username == "restaurant") {
        header("Location: restaurant_index.php");
        exit();
    } elseif ($username == "delivery") {
        header("Location: delivery_index.php");
        exit();
    }

    // Assuming you have user data stored in a session or database
    // Replace the following lines with the actual user data retrieval logic
    $userData = [
        'name' => 'John Cena', // Replace with the user's name
        'phone' => '21800000', // Replace with the user's phone number
        'address' => '123 Main St', // Replace with the user's address
        'district' => 'Kowloon City' // Replace with the user's district
    ];
}
?>

<div class="container">
    <div class="checkoutLayout">


        <div class="returnCart">
            <a href="\itp4506_hci\ITP4506_HCI\order.php" id="keep-shopping">Keep Shopping</a>
            <h1>List Product in Cart</h1>
            <div class="list">

                <div class="item">
                    <img src="italian/1.PNG">
                    <div class="info">
                        <div class="name">PRODUCT 1</div>
                        <div class="price">$12</div>
                    </div>
                    <div class="quantity">2</div>
                    <div class="returnPrice">$24</div>
                </div>

            </div>
        </div>


        <div class="right">
            <h1>Checkout</h1>
            <div class="form">
                <form id="checkoutForm" action="payment.php" method="post">
                    <div class="group">
                        <label for="name">Full Name</label>
                        <input type="text" name="name" id="name" value="<?php echo $username ? 'John Cena' : ''; ?>"
                               required>
                    </div>

                    <div class="group">
                        <label for="phone">Phone Number</label>
                        <input type="text" name="phone" id="phone" value="<?php echo $username ? '21800000' : ''; ?>"
                               required>
                    </div>

                    <div class="group">
                        <label for="address">Address</label>
                        <input type="text" name="address" id="address"
                               value="<?php echo $username ? '123 Main St' : ''; ?>" required>
                    </div>

                    <div class="district_group">
                        <label for="district">District</label>
                        <select name="district" id="district" required>
                            <option value="">Choose..</option>

                            <?php
                            // Array of districts organized by region
                            $districts = [
                                'New Territories' => ['Islands', 'Kwai Tsing', 'North', 'Sai Kung', 'Sha Tin', 'Tai Po', 'Tsuen Wan', 'Tuen Mun', 'Yuen Long'],
                                'Kowloon' => ['Kowloon City', 'Kwun Tong', 'Sham Shui Po', 'Wong Tai Sin', 'Yau Tsim Mong'],
                                'Hong Kong Island' => ['Central and Western', 'Eastern', 'Southern', 'Wan Chai'],
                            ];

                            foreach ($districts as $region => $districtList) {
                                echo "<optgroup label=\"$region\">";
                                foreach ($districtList as $district) {
                                    $selected = ($username && $userData['district'] == $district) ? 'selected' : '';
                                    echo "<option value=\"$district\" $selected>$district</option>";
                                }
                                echo "</optgroup>";
                            }
                            ?>

                        </select>
                    </div>

            </div>
            <div class="return">
                <div class="row">
                    <div>Total Quantity</div>
                    <div class="totalQuantity">2</div>
                </div>
                <div class="row">
                    <div>Total Price</div>
                    <div class="totalPrice">$24.0</div>
                </div>
            </div>
            <button type="submit" class="buttonCheckout">CHECKOUT</button>
            <script>
                function redirectToPayment() {
                    window.location.href = 'payment.php';
                }
            </script>
            </form>
        </div>
    </div>
</div>

</body>
</html>
<body>

</body>
</html>