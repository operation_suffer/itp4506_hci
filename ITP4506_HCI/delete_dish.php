<?php
session_start();

// Check if the user is logged in
if (!isset($_SESSION["username"]) || $_SESSION["username"] == "user" || $_SESSION["username"] == null) {
    header("Location: index.php");
    exit();
}

// Check if the dish ID is provided in the URL
if (isset($_GET['dish'])) {
    $dishId = $_GET['dish'];

    // Load the existing menu data
    $menuJson = file_get_contents('italian/italian_menu.json');
    $menuData = json_decode($menuJson, true);

    // Check if the dish with the given ID exists
    if (isset($menuData[$dishId])) {
        // Remove the dish from the menu
        unset($menuData[$dishId]);

        // Save the updated menu back to the JSON file
        file_put_contents('italian/italian_menu.json', json_encode($menuData, JSON_PRETTY_PRINT));

        // Redirect back to the menu page after deletion
        header("Location: restaurant_menu.php");
        exit();
    } else {
        // Dish not found
        echo "Error: Dish not found.";
    }
} else {
    // Dish ID not provided
    echo "Error: Dish ID not provided.";
}
?>
