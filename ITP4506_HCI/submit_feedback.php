<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get the feedback and rating from the form
    $feedback = $_POST["feedback"];
    $rating = $_POST["rating"];

    // TODO: Save feedback and rating to your database or storage

    // Optionally, you can redirect the user to a thank you page
    header("Location: thank_you.php");
    exit();
} else {
    // Redirect to the homepage if the form is not submitted
    header("Location: index.php");
    exit();
}

